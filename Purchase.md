## Google Play Billing Library
*Google Play Billing* is a service that lets you sell digital content from inside an Android app, or in-app. 

3 types of purchases are there 
1. *One-time products*: An in-app product requiring a single, non-recurring charge to the user's form of payment. Additional game levels, premium loot boxes, and media files are examples of one-time products. The Google Play Console refers to one-time products as managed products, and the Google Play Billing library calls them "INAPP".

2. *Rewarded products*: An in-app product requiring the user to watch a video advertisement. Extra lives, in-game currency, and quick completion of timed tasks are examples of rewarded products. The Google Play Console refers to rewarded products as rewarded products, and the Google Play Billing Library calls them "INAPP".

3. *Subscriptions*: An in-app product requiring a recurring charge to the user's form of payment. Online magazines and music streaming services are examples of subscriptions. The Google Play Billing Library calls these "SUBS".


* Google Play Billing tracks products and transactions using purchase tokens and order IDs.

* For one-time products and rewarded products
    * Every purchase creates a new token and a new order ID.

* For subscriptions: 
    * An initial purchase creates a purchase token and an order ID. For each continuous billing period, the purchase token stays the same and a new order ID is issued. 
    * Upgrades, downgrades, and re-signups all create new purchase tokens and order IDs.

For implementation of in app purchase,
[Click here](https://droidmentor.com/inapppurchase-subscription/)   

#### The following is a list of the high-level building blocks of a Google Play Billing solution:

1. Google Play. An online store where users can download digital products.
2.  [Google Play Console](https://developer.android.com/distribute/console/index.html) . The interface app developers use to publish apps on Google Play. App developers use the Google Play Console to identify details about their app, including any in-app products. For more information, refer to The Google Play Console.
3.  [Google API Console](https://console.developers.google.com/) . The console for managing backend APIs, such as the [Google Play Developer API](https://developers.google.com/android-publisher/) . Create service accounts here to verify purchases and subscriptions from your secure backend server.
4. Android device. Any device used to run Android apps, such as a tablet or mobile phone.
5. Android app. An application intended to run on an Android device.
6. Secure backend server. A developer-provided server used to implement purchase verification or subscription features, such as [real-time developer notifications](https://developer.android.com/google/play/billing/realtime_developer_notifications.html) .
7. The Play Store app. The app responsible for managing all operations related to the Google Play Billing. All requests made by your app will be handled by the Play Store app.
8.  [Google Play Billing Library](https://developer.android.com/google/play/billing/billing_library_overview) . An API that developers use to implement Google Play Billing within an app.
9.  [Google Play Developer API](https://developers.google.com/android-publisher/) . A REST API used to programmatically perform a number of publishing and app-management tasks. It includes two components, the [Subscriptions and In-App Purchases API](https://developers.google.com/android-publisher/#subscriptions) used to manage in-app purchases and subscriptions, and the [Publishing API](https://developers.google.com/android-publisher/#publishing) to upload and publish apps, and other publishing-related tasks.
10.  [Real-time developer notifications](https://developer.android.com/google/play/billing/realtime_developer_notifications) . Server push notifications that let you monitor state changes, such asSUBSCRIPTION_PURCHASEDorSUBSCRIPTION_RECOVERED, for Play-managed subscriptions.
