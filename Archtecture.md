# Architecture

Common architecture followed in android are three MVC MVP and MVVM 

### Model
The Model represents a set of classes that describes the business logic and data. It also defines business rules for data means how the data can be changed and manipulated.


### View
The component that handles input-output from/to the user. This component is referred to as View.

The view has a responsibility to render the User Interface (UI) and communicate to the controller when the user interacts with the application. 

### Controller / Presenter
The component that encapsulates the logical functionality of the system. This component is referred to as Controller/Presenter.

When the View tells the controller that a user clicked a button, the controller decides how to interact with the model accordingly. Based on data changing in the model, the controller may decide to update the state of the view as appropriate. In the case of an Android application, the controller is almost always represented by an Activity or Fragment.

### Presenter
The Presenter is responsible for handling all UI events on behalf of the view. This receive input from users via the View, then process the user’s data with the help of Model and passing the results back to the View. Unlike view and controller, view and presenter are completely decoupled from each other’s and communicate to each other’s by an interface.
Also, presenter does not manage the incoming request traffic as controller.

### View Model
The ViewModel is responsible for exposing methods, commands, and other properties that helps to maintain the state of the view, manipulate the model as the result of actions on the view, and trigger events in the view itself.


#### MVC 

MVC pattern is Standard Android pattern. This is the "default" approach with layout files, Activities/Fragments acting as the controller and Models used for data and persistence.With this approach, Activities are in charge of processing the data and updating the views. Activities act like a controller in MVC but with some extra responsibilities that should be part of the view. 

![MVC](https://cdn-images-1.medium.com/max/800/0*HFP--PRKvRXsS7fd.png)

##### Advantage 
* The Model-View-Controller pattern highly supports the separation of concerns. This advantage not only increases the testability of the code but it also makes it easier to extend, allowing a fairly easy implementation of new features.

* The Model classes don’t have any reference to Android classes and are therefore **straightforward** to unit test

##### Disadvantage

* The View has a reference to both the Controller and the Model

* It does not limit the handling of UI logic to a single class, this responsibility being shared between the Controller and the View or the Model.


#### MVP
MVP is dervied from MVC; gets all goodness from MVC and adds it own flavor. Difference  between MVP and MVC is that MVP uses interfaces pattern for communication and remove the view's communicaiton with model.

![MVP](https://cdn-images-1.medium.com/max/1400/0*VOr9hgXEmoTxyoyz.png)

#### Advantage
* MVP breaks the View's connection with the Model and creating only one class that handles everything related to the presentation of the View — the Presenter: a single class that is easy to unit test.

#### Disadvantage 
* When developing a small app or a prototype, this can seem like an overhead. To decrease the number of interfaces used, some developers remove the Contract interface class, and the interface for the Presenter.

* One of the pitfalls of MVP appears when moving the UI logic to the Presenter: this becomes now an all-knowing class, with thousands of lines of code. To solve this, split the code even more and remember to create classes that have only one responsibility and are unit testable.
#### MVVM

MVVM combines the advantages of separation of concerns provided by MVP, while leveraging the advantages of data bindings. The result is a pattern where the model drives as many of the operations as possible, minimizing the logic in the view.

![MVVM](https://cdn-images-1.medium.com/max/1600/0*5mD214cjNXU-V6lf.png)



