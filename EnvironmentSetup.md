# Environmental Setup

### What is Environmental Setup?
Setting up app to use different credentials and API endpoints with the environment our choice like dev, staging and production.

![Environment](Images/Environment1.jpg)

### Why it is needed?

Lets consider you implemented a feature to send notification to all your user on every monday and the application went to production. There is a issue in notification module that istead of monday it sents notification on friday and sunday. 

How will you test this issue is fixed or not? Will you sent notification to live users? 

This is why we need diffrent environment.

![Environment](Images/Environment2.jpg)

### What are the things comes under Environment?

This is based on the project. Broadly if something comes under this it should have diffrent environment. 
1. OAuth
    1. Google 
    2. FaceBook
    3. Twitter 
    4. Linkedin
    5. Instagram
2. API endpoints
3. Notification
4. Force updates
5. Remote Config 
6. Fabric distribution channels
7. Analytics
8. Deep Links and Share urls

>OAuth:You can set environment OAuth use different project 

### Project Specific details:

|   	|   Dev	|   Staging	|   Release	|
|---	|---	|---	|---	|
|   Google signup 	|   Staging creds	|  Staging creds 	|   Production creds	|
|   Facebook signup	|    Staging creds	|  Staging creds 	|   Production creds	|
|   Crashlytics 	|   nfnlabs.design.nfwalls.debug	| nfnlabs.design.nfwalls.staging  	|  nfnlabs.design.nfwalls 	|   	|
|`nfnlabs.design` API's |   	https://staging.nfnlabs.design |   https://staging.nfnlabs.design	|  https://nfnlabs.design 	|
| App and Image Share url|   https://stagingnfwalls.page.link	|   https://stagingnfwalls.page.link	|  https://nfwalls.page.link 	|
|Deeplinkurl|  https://staging.nfnlabs.design/?query={token}|    https://nfnlabs.design/?query={token}	|  https://nfnlabs.design/?query={token}	|

